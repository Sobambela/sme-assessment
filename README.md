
## Requeirements

1. PHP > 7.2
2. Apache2
3. ModRewrite enabled

## Set up

1. Clone the repo in a document root of your webserver
`````
$ git clone https://gitlab.com/Sobambela/sme-assessment.git

`(The composer dependecies are included in the repo so there is no need to run composer install)`
`````
## API

The API consist of one endpoint ```/checkins``` .
To run the solution send a POST request with the json test/sample data (````/storage/file.js````) located in the root of the project.

### Example Request 

````
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://demo.test/checkins',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS =>'{     
    "data" : [
        {
            "id": 1,
            "tenant_id": 1001,
            "first_name": "Yongama",
            "surname": "Sobambela",
            "email_address": "sobambela@gmail.com",
            "telephone_number": "012123456",
            "move_in_date": "2023-01-1",
            "move_in_time": "17:30",
            "property_id": 1
        },
        {
            "id": 2,
            "tenant_id": 1002,
            "first_name": "Yongama",
            "surname": "Sobambela",
            "email_address": "sobambela@gmail.com",
            "telephone_number": "012123456",
            "move_in_date": "2023-01-26",
            "move_in_time": "20:00",
            "property_id": 2
        },
        ...
    ]
}',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: text/plain'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;

````

### Example Response

```
{
    "checkins": {
        "manager-a": [
            null,
            [
                {
                    "id": 4,
                    "tenant_id": 1004,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-7",
                    "move_in_time": "01:30",
                    "property_id": 4
                }
                ...
            ]
        ],
        "manager-b": [
            [
                {
                    "id": 12,
                    "tenant_id": 10021,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-15",
                    "move_in_time": "09:00",
                    "property_id": 11
                },
                {
                    "id": 29,
                    "tenant_id": 19,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-15",
                    "move_in_time": "16:30",
                    "property_id": 26
                }
            ],
            {
                "id": 1,
                "tenant_id": 1001,
                "first_name": "Yongama",
                "surname": "Sobambela",
                "email_address": "sobambela@gmail.com",
                "telephone_number": "012123456",
                "move_in_date": "2023-01-1",
                "move_in_time": "17:30",
                "property_id": 1
            },
            {
                "id": 16,
                "tenant_id": 12,
                "first_name": "Yongama",
                "surname": "Sobambela",
                "email_address": "sobambela@gmail.com",
                "telephone_number": "012123456",
                "move_in_date": "2023-01-11",
                "move_in_time": "11:30",
                "property_id": 16
            },
            ...
        ],
        "manager-c": [
            [
                {
                    "id": 16,
                    "tenant_id": 12,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-11",
                    "move_in_time": "11:30",
                    "property_id": 16
                },
                {
                    "id": 25,
                    "tenant_id": 14,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-11",
                    "move_in_time": "06:00",
                    "property_id": 22
                },
                ...
            ]
        ],
        "last-day-of-the-month": {
            "29": {
                "id": 30,
                "tenant_id": 20,
                "first_name": "Yongama",
                "surname": "Sobambela",
                "email_address": "sobambela@gmail.com",
                "telephone_number": "012123456",
                "move_in_date": "2023-01-31",
                "move_in_time": "02:00",
                "property_id": 27
            }
        },
        "contact-for-another-appointment": [
            [
                {
                    "id": 17,
                    "tenant_id": 6,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-4",
                    "move_in_time": "22:00",
                    "property_id": 15
                },
                {
                    "id": 18,
                    "tenant_id": 7,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-4",
                    "move_in_time": "22:00",
                    "property_id": 16
                }
            ],
            [
                {
                    "id": 19,
                    "tenant_id": 8,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-14",
                    "move_in_time": "12:00",
                    "property_id": 17
                },
                {
                    "id": 22,
                    "tenant_id": 11,
                    "first_name": "Yongama",
                    "surname": "Sobambela",
                    "email_address": "sobambela@gmail.com",
                    "telephone_number": "012123456",
                    "move_in_date": "2023-01-14",
                    "move_in_time": "15:00",
                    "property_id": 20
                }
            ]
        ]
    },
    "errors": []
}
```