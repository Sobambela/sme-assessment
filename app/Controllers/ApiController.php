<?php

namespace App\Controllers;

use App\Database\ApiGateway;
use App\Models\Managers;

 class ApiController
 {
     public function __construct()
     {
     }

     public function processRequest(string $method, string $route,?string $id): void
     {
         if($route != 'managers' && $route != 'applications' && $route != 'checkins'){
             http_response_code(404);
             exit;
         }

         if($id){
             $this->handleResourceRequest($method, $route, $id);
         }else{
             $this->handleCollectionRequest($method, $route);
         }
     }

     private function handleResourceRequest(string $method, string $route, $id): void
     {
         // Dynamically create Models based on the Route
         $model = $this->loadModel( $route);
         $item = $model->get($id);

         if( ! $item){
             http_response_code(404);
             echo json_encode(["message" => "Not found"]);
             return;
         }

         switch ($method) {
             case "GET":
                 echo json_encode($item);
                 break;
             case "PATCH":
                 $data = (array) json_decode(file_get_contents("php://input"), true);

                 $errors = $model::validate($data, true);

                 if( ! empty($errors)){
                     http_response_code(422);
                     echo json_encode(['errors' => $errors]);
                     break;
                 }

                 $rows = $model->update($item, $data);

                 http_response_code(200);
                 echo json_encode([
                     'message' => "Item $id updated",
                     'rows' => $rows
                 ]);
                 break;
             case "DELETE":
                 $rows = $model->delete($id);
                 echo json_encode([
                     'message' => "Item $id delete",
                     'rows' => $rows
                 ]);
                 break;
             default:
                 http_response_code(405);
                 header("Allow: GET, PATCH, DELETE");
         }
     }

     private function handleCollectionRequest(string $method, string $route): void
     {
         // Dynamically create Models based on the Route
         $model = $this->loadModel($route);

         switch ($method) {
             case "POST":
                 $data = (array) json_decode(file_get_contents("php://input"), true);

                 $errors = $model->validate($data['data']);

                 $checkins = $model->calculateCheckins($data['data']);

                 http_response_code(201);
                 echo json_encode([
                     'checkins' => $checkins,
                     'errors' =>  $errors
                 ]);
                 break;
             default:
                 http_response_code(405);
                 header("Allow: POST");
         }
     }

     public function loadModel(string $className)
     {
         $model = ucfirst( $className );
         $model =  "\\App\\Models\\{$model}";

         return new $model();
     }
}