<?php

namespace App\Models;

use PDO;
use \App\Database\ApiGateway;

class Checkins extends ApiGateway
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Validate the check in application for the required move in and date and time
     *
     * @param string $data
     *
     * @return array
     */
    public static function validate(array $data): array
    {
        $errors = [];

        foreach ( $data as $item ){
            if(empty($item['move_in_date'])) {
                $errors[] = "Tenant ID {$item['tenant_id']}'s move in in date is required";
            }
            if(empty($item['move_in_time'])) {
                $errors[] = "Tenant ID {$item['tenant_id']}'s move in in time is required";
            }
        }

        return $errors;
    }

    /**
     * Calculates a list of properties  to visit and if need be splits it up among 3 managers
     *
     * @param string $checkInApplications
     *
     * @return array
     */
    public function calculateCheckins(array $checkInApplications): array
    {
        $managerABatch = [];
        $managerBBatch = [];
        $managerCBatch = [];
        $lastDayOfTheMonthBatch = [];
        $toContactForAnotherAppointment = [];

        $lastDayOfTheMonthBatch = array_filter($checkInApplications, function($item) {
            return $this->isLastDayOfMonth($item['move_in_date']);
        });

        $checkInApplications = $this->removeFromTheseFromOriginal($checkInApplications, $lastDayOfTheMonthBatch);

        $conflicts =  $this->findDuplicateItemsByTwoIndexes($checkInApplications, 'move_in_time','move_in_date');

        if(count($conflicts) > 3){
            $managerABatch[] = $conflicts[0];
            $managerBBatch[] = $conflicts[1];
            $managerCBatch[] = $conflicts[2];
            for ($i = 3; $i <= count($conflicts); $i++){
                $toContactForAnotherAppointment[] = $conflicts[$i];
            }
        } else if(count($conflicts) == 3){
            $managerABatch[] = $conflicts[0];
            $managerBBatch[] = $conflicts[1];
            $managerCBatch[] = $conflicts[2];
        } else if(count($conflicts) == 2){
            $managerABatch[] = $conflicts[0];
            $managerBBatch[] = $conflicts[1];
        } else if(count($conflicts) == 1){
            $managerABatch[] = $conflicts[0];
        }
        $checkInApplications = $this->removeFromTheseFromOriginal($checkInApplications, $toContactForAnotherAppointment);

        // Same property check in on the same day
        $samePropertyCheckInSameDay = $this->findDuplicateItemsByIndex($checkInApplications, 'move_in_date');

        if(count($samePropertyCheckInSameDay) > 3) {
            $counter = 0;
            foreach ($samePropertyCheckInSameDay as $key => $item){
                if($counter > 2){
                    $toContactForAnotherAppointment[] = $item;
                }else if($counter == 2){
                    $managerCBatch[] = $item;
                }else if($counter == 1){
                    $managerBBatch[] = $item;
                }else if ($counter == 0){
                    $managerABatch[] = $item;
                }
                $counter++;
            }
        }

        $checkInApplications = $this->removeFromTheseFromOriginal($checkInApplications, $samePropertyCheckInSameDay);

        $sortedApplications = $this->sortByIndex($checkInApplications, 'move_in_date');
        if(!empty($managerABatch)){
            $managerABatchSorted = $this->sortByIndex($managerABatch, 'move_in_date');
        }else{
            $managerABatchSorted = [];
        }
        foreach ($sortedApplications as $checkIn){
            $lastAppointment = end($managerABatchSorted);
            if(!empty($lastAppointment)){

                if( isset($lastAppointment['move_in_date']) && $lastAppointment['move_in_date'] ==  $checkIn['move_in_date']){
                    if($this->timeDifferenceGreaterThan30Minutes( $lastAppointment['move_in_time'], $checkIn['move_in_time'] )){
                        $managerBBatch[] = $checkIn;
                    }
                }else{
                    $managerBBatch[] = $checkIn;
                }
            }else{
                $managerABatch[] = $checkIn;
            }
            $managerABatchSorted = $managerBBatch;
        }

        return [
                'manager-a' => $managerABatch,
                'manager-b' => $managerBBatch,
                'manager-c' => $managerCBatch,
                'last-day-of-the-month' => $lastDayOfTheMonthBatch,
                'contact-for-another-appointment' => $toContactForAnotherAppointment
        ];
    }

    public function findDuplicateItemsByIndex(array $arr, string $index): array
    {
        $duplicates = array();

        foreach ($arr as $item) {
            if (isset($duplicates[$item[$index]])) {
                $duplicates[$item[$index]][] = $item;
            } else {
                $duplicates[$item[$index]] = array($item);
            }
        }

        return array_filter($duplicates, function($item) {
            return count($item) > 1;
        });
    }

    public function findDuplicateItemsByTwoIndexes($arr, $firstIndex, $secondIndex): array
    {
        $duplicates = array();

        foreach ($arr as $item) {
            $key = $item[$firstIndex] . "-" . $item[$secondIndex];
            if (isset($duplicates[$key])) {
                $duplicates[$key][] = $item;
            } else {
                $duplicates[$key] = array($item);
            }
        }

        return array_filter($duplicates, function($item) {
            return count($item) > 1;
        });
    }

    public function isLastDayOfMonth(string $date): bool
    {
        $currentDate = new \DateTime($date);
        $nextDate = new \DateTime($date);
        $nextDate->modify("+1 day");
        return $currentDate->format("m") != $nextDate->format("m");
    }

    public function removeFromTheseFromOriginal(array $original, array $toRemove ): array
    {
        foreach ($toRemove as $key => $item){
            unset($original[$key]);
        }

        return $original;
    }

    public function  sortByIndex($array, $index)
    {
          // create a custom sort function
          $sort_function = function($a, $b) use ($index) {
            return strcmp($a[$index], $b[$index]);
          };

        // sort the array using the custom sort function
        usort($array, $sort_function);

        return $array;
    }

    public function timeDifferenceGreaterThan30Minutes(string $time1, string $time2): bool
    {
        $time1 = strtotime($time1);
        $time2 = strtotime($time2);

        $difference = abs($time1 - $time2);

        return $difference > 30 * 60;
    }
}