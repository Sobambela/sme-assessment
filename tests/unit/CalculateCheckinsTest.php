<?php

use \PHPUnit\Framework\TestCase;
use App\Models\Checkins;

class CalculateCheckinsTest extends TestCase
{
    public function test_calculate_checkins()
    {
        $applications = new Checkins();

        // Mock the json that will be coming from the POST request by reading the sample data json file
        $jsonPath = __DIR__.'/../../storage/file.json';
        // Read the JSON file
        $json = file_get_contents($jsonPath);

        // Decode the JSON file
        $applicationsData = json_decode($json,true);


        $checkIns = $applications->calculateCheckins($applicationsData['data']);

        // Assert if a json object (string) is returned
        $this->assertIsArray($checkIns);

        // Assert if at least one manager was allocated check ins
        $this->assertArrayHasKey('manager-a', $checkIns, "Array doesn't contains 'manager-a' as key");
    }

}