<?php
declare(strict_types=1);

require __DIR__ . "/vendor/autoload.php";

use App\Controllers\ApiController;

header("Content-type: application/json; charset=UTF-8");

$uriParts = explode('/', $_SERVER["REQUEST_URI"]);
$route = $uriParts[1];
$id = $uriParts[2] ?? null;

$app = new ApiController();

$app->processRequest($_SERVER["REQUEST_METHOD"], $route, $id);